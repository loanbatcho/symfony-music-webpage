<?php

namespace App\Controller\api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;

class SongController extends AbstractController
{
    #[Route(path: "/api/song/{id<\d+>}", name: "api_getSong")]
    public function getSong(int $id, LoggerInterface $logger): Response
    {
        $song = [
            "id" => $id,
            "artist" => "Damso",
            "url" => "https://shit-to-get",
        ];
        $logger->info("Returning song  info ");
        return $this->json($song);
    }
}
